#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 18 15:20:52 2019

GPU-processing (Numba) implementation of
    the Statistical Region Merging algorithm
    from Richard Nock and Frank Nielsen (2004)

@author: Vincent Mahé <vmahe@free.fr>

@license: GNU GPL v2 or greater
"""
import numpy
import numba
import math

from UnionFind import ufFind, ufUnion

@numba.jit
def merge(Si, picture, Q):
    # the array to store current root of each pixel
    # 3rd dim: (x,y) coordinates of region root + number of elements in region
    roots = numpy.empty(shape=(picture.shape[0],picture.shape[1],3), dtype=numpy.int32)
    
    # UnionFind: depth array
    ufD = numpy.empty(shape=(picture.shape[0],picture.shape[1]), dtype=numpy.int32)
    
    # initially, each pixel is its own region
    for x in range(picture.shape[0]):
        for y in range(picture.shape[1]):
            roots[x, y, 0] = x
            roots[x, y, 1] = y
            roots[x, y, 2] = 1
            ufD[x, y] = 1
    
    # first merging pass
    # NB: UnionFind is recursive and compresses tree so this get large regions
    for i in range(Si.shape[0]):
        # find the root corresponding to each edge element
        x1, y1 = ufFind(Si[i,0], Si[i,1], roots)
        x2, y2= ufFind(Si[i,2], Si[i,3], roots)
        
        if (x1 != x2 or y1 != y2) and predicate(picture, roots, Q, x1, y1, x2, y2):
            mergeRegions(picture, roots, ufD, x1, y1, x2, y2)
    
    # NB: original implementation managed small regions
    # We do not, for the moment
    
    return roots
    
"""
 implementation of the SRM 2004 paper formula:
     |\bar{R'}| - |\bar{R}| \leq \sqrt{b^2 (R)+b^2 (R')}
 where
     b(R) = g \sqrt{(1/(2 Q |R|)) \ln{(|\mathcal{R}_l| / \delta})}
 
 Rem: the predicate is 'squared' to avoid square root computations:
     (|\bar{R'}| - |\bar{R}|)^2 \leq b^2 (R)+b^2 (R')
 and
     b^2(R) = (g^2 / (2 Q |R|)) (\ln{(|\mathcal{R}_l|)} + ln{(inv \delta)})
"""
@numba.jit
def predicate(picture, roots, Q, x1, y1, x2, y2):
    # 'g' parameter represents the end of values interval for R, G, B
    g = 256.0
    
    # '|R|' is computed when merging regions
    # (1 as default when each pixel is a region)
    nR1 = roots[x1, y1, 2]
    nR2 = roots[x2, y2, 2]
    
    # the /R|R|/ parameter is determined by
    # |\mathcal{R}_l| \leq (l+1)^{min\{l,g\}}
    # and \ln( (l+1)^{min\{l,g\}}) = min\{l,g\} \ln(l+1)
    lnRR1 = min(g, nR1) * math.log(1 + nR1)
    lnRR2 = min(g, nR2) * math.log(1 + nR2)
    
    # 'delta' parameter is not fully detailled in paper,
    # except that ''>= 1- (N delta)' is a probability so is < 1
    # I do not know from where come the '2*' and '6' factors present in all implementations
    # delta = 1. / (6 * h * w)
    lnInvDelta = 2 * math.log(6 * picture.shape[0] * picture.shape[1])
    
    # we have to compute difference for R, G and B channels
    # average color value for each region has been stored in root pixel
    diffR = picture[x2, y2, 0] - picture[x1, y1, 0]
    diffG = picture[x2, y2, 1] - picture[x1, y1, 1]
    diffB = picture[x2, y2, 2] - picture[x1, y1, 2]
    
    b2R1 = (g * g / (2 * Q * nR1)) * (lnRR1 + lnInvDelta)
    b2R2 = (g * g / (2 * Q * nR2)) * (lnRR2 + lnInvDelta)
    
    b2RR = b2R1 + b2R2
    
    return (diffR * diffR < b2RR) and (diffG * diffG < b2RR) and  (diffB * diffB < b2RR)

# compute average R, G, B and store them in root pixel
@numba.jit
def mergeRegions(picture, roots, ufD, x1, y1, x2, y2):
    # Rem: the RGB values of the root pixel stores the average values for its region
    
    xR, yR = ufUnion(roots, ufD, x1, y1, x2, y2)
    
    n1 = roots[x1, y1, 2]
    n2 = roots[x2, y2, 2]
    nRegion = n1 + n2
    
    rRegion = int((n1 * picture[x1, y1, 0] + n2 * picture[x2, y2, 0]) / nRegion)
    gRegion = int((n1 * picture[x1, y1, 1] + n2 * picture[x2, y2, 1]) / nRegion)
    bRegion = int((n1 * picture[x1, y1, 2] + n2 * picture[x2, y2, 2]) / nRegion)
    
    # register the new root in both pixels
    roots[x1, y1, 0] = xR
    roots[x1, y1, 1] = yR
    roots[x2, y2, 0] = xR
    roots[x2, y2, 1] = yR
    
    # update root values
    roots[xR, yR, 2] = nRegion
    picture[xR, yR, 0] = rRegion
    picture[xR, yR, 1] = gRegion
    picture[xR, yR, 2] = bRegion
