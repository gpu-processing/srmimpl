#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 19 13:18:49 2019

GPU-processing (Numba) implementation of
    the Statistical Region Merging algorithm
    from Richard Nock and Frank Nielsen (2004)

@author: Vincent Mahé <vmahe@free.fr>

@license: GNU GPL v2 or greater
"""

import numba

from UnionFind import ufFind

@numba.jit
def reassess(picture, roots):
    h = picture.shape[0]
    w = picture.shape[1]
    
    # the average values for region RGB color are stored
    # in the root pixel, within 'picture'
    for x in range(h):
        for y in range(w):
            xR, yR = ufFind(x, y, roots)
            
            picture[x, y, 0] = picture[xR, yR, 0]
            picture[x, y, 1] = picture[xR, yR, 1]
            picture[x, y, 2] = picture[xR, yR, 2]
    
    return picture
