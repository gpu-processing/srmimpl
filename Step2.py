#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 17 13:27:52 2019

GPU-processing (Numba) implementation of
    the Statistical Region Merging algorithm
    from Richard Nock and Frank Nielsen (2004)

@author: Vincent Mahé <vmahe@free.fr>

@license: GNU GPL v2 or greater
"""

import numpy
import numba

def sort(Si):
    
    # indexes of Si element, ordered by distance
    rankedSi = numpy.zeros(Si.shape, dtype=numpy.int32)
    
    bucketSort(Si, rankedSi)
    
    return rankedSi

@numba.jit
def bucketSort(Si, rankedSi):
    """
    As distances are integers between 0 and 255, we can use
    a 'bucket sort' to get edges in the distance order
    """
    bIndex = numpy.zeros(shape=(256,), dtype=numpy.int32)
    
    #first pass: count edges in the buckets
    i = 0
    while i < Si.shape[0]:
        dist = Si[i, 4]
        bIndex[dist] += 1
        i += 1
    
    #second pass: compute first abcisse for each dif category
    abcisses = numpy.zeros(shape=(256,), dtype=numpy.int32)
    
    # as each bIndex is the number of edges in the category,
    # we must set the first value of the second index to bIndex[0]-1
    currentAbc = -1
    for j in range(1,256):
        currentAbc += bIndex[j-1]
        abcisses[j] = currentAbc
    
    #third pass: reorder Si
    i = 0
    while i < Si.shape[0]:
        # dist gives the category
        dist = Si[i,4]
        newRank = abcisses[dist]
#        if abcisse > 14150380:
#            print(Si[i,:])
        reorderSi(Si, rankedSi, i, newRank)
        # we increment abcisse of the category
        abcisses[dist] += 1
        i += 1

@numba.jit
def reorderSi(Si, rankedSi, i, j):
    rankedSi[j, 0] = Si[i, 0]
    rankedSi[j, 1] = Si[i, 1]
    rankedSi[j, 2] = Si[i, 2]
    rankedSi[j, 3] = Si[i, 3]
    rankedSi[j, 4] = Si[i, 4]
    