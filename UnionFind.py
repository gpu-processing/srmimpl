#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 19 20:21:39 2019

GPU-processing (Numba) implementation of
    the Statistical Region Merging algorithm
    from Richard Nock and Frank Nielsen (2004)

@author: Vincent Mahé <vmahe@free.fr>

@license: GNU GPL v2 or greater
"""
import numba

# Tarjan's UnionFind straucture & algorithm
@numba.jit
def ufFind(x, y, roots):
    xR = roots[x, y, 0]
    yR = roots[x, y, 1]
    
    # compression algorithm
    if x == xR and y == yR:
        # we found the region root
        return x, y
    else:
        # we get the parent root
        return ufFind(xR, yR, roots)

# register the merge of two region trees
@numba.jit
def ufUnion(roots, ufD, x1, y1, x2, y2):
    # which tree will be the new one?
    if ufD[x1, y1] > ufD[x2, y2]:
        # tree2 is inserted in tree1
        roots[x2, y2, 0] = x1
        roots[x2, y2, 1] = y1
        return x1, y1
    else:
        # tree1 inserted in tree2
        roots[x1, y1, 0] = x2
        roots[x1, y1, 1] = y2
        if ufD[x1, y1] == ufD[x2, y2]:
            # the two trees had the same depth 
            # so the new root is one step deeper
            ufD[x2, y2] += 1
        return x2, y2
