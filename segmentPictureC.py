#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 14 16:31:48 2019

Main program for running the implementation of
    the Statistical Region Merging algorithm
    from Richard Nock and Frank Nielsen (2004)

@author: Vincent Mahé <vmahe@free.fr>

@license: GNU GPL v2 or greater
"""

import sys
import imageio   # requires Pillow library
#import matplotlib.pyplot as mplt
import numpy

from Step1C import buildSi
from Step2 import sort
from Step3 import merge
from Step4 import reassess

# Load the input image, run segmentation on it, and generate segmented image
def segmentPicture(pictureFileName, Q):
    
    #### load input picture
    picture = imageio.imread(pictureFileName)
    
#    mplt.imshow(picture)
    
    #push uint8 picture to int32 to avoid overflow problems
    pict32 = numpy.empty(shape=picture.shape, dtype=numpy.int32)
    numpy.copyto(pict32, picture, casting='safe')
    
    ###### SRM algorithm implementation ######
    print('\n******* compute edges *******')
    # generate the Si set of couples of adjacent pixels
    Si = buildSi(pict32)
    
    print('\n******* sort edges *******')
    # sort Si couples in increasing order of f(.,.)
#    Si = sort(Si)
    
    print('\n******* merge regions *******')
    # compute root of the region tree for each pixel
    Q = 100  # Q in [1,256] : the higher, the more regions you get
    roots = merge(Si, pict32, Q)
    # NB: pictureArray now contains average RGB values for each tree root
    
    print('\n******* reassess pixels *******')
    # put the region average RGB values to its pixels
    segmentedPictureArray = reassess(pict32, roots)
    
    #### output the segmented picture
    segmentedPicture = numpy.empty(shape=pict32.shape, dtype=numpy.uint8)
    numpy.copyto(segmentedPicture, segmentedPictureArray, casting='unsafe')
    
#    print('\nmplt.imshow(segmentedPicture)\n')
#    mplt.imshow(segmentedPicture)
    
    segmentedFileName = "seg_XY" + pictureFileName
    
    imageio.imwrite(segmentedFileName, segmentedPicture)
    
    return segmentedFileName

# manage the console runs
if __name__ == "__main__":
    if len(sys.argv) < 2:
        print ('Usage:\n python segmentPicture.py pictureFileName [40]\nwhere 40 is Q in [1, 256]')
#        sys.exit
    else:
        if len(sys.argv) == 2:
            Q = 1
            print('\nDefault Q = {}\n'.format(Q))
        else:
            Q = sys.argv[2]
        
        outFileName = segmentPicture(sys.argv[1], Q)
        print('Segmented picture save as {}'.format(outFileName))
