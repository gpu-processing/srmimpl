#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 14 21:39:42 2019

GPU-processing (Numba) implementation of
    the Statistical Region Merging algorithm
    from Richard Nock and Frank Nielsen (2004)

@author: Vincent Mahé <vmahe@free.fr>

@license: GNU GPL v2 or greater
"""

import numba
import numpy

"""
Build the Si set of couples of adjacent pixels, considering
    the 4-connexity, where each pixel is related to 
    its left neighbour and its below neighbour.
As NamedTuple and classes are not managed in Numba,
    we directly compute the distance(s) within each couple
    as we create corresponding Si ~N*2 tuples.
"""
def buildSi(picture):
    
    h = picture.shape[0]
    w = picture.shape[1]
    
    # see if NaN in picture
#    print(picture[numpy.isnan(picture)])
    
#    gpuPicture = cuda.to_device(picture)
#    # grid dimensions for the kernel functions
#    blockPicDim = 32, 32
#    gridPicDim = int(w/32) +1, int(h/32) + 1
    
    # C4-connectivity pairs
    nRE = (h) * (w - 1)
    nBE = (h - 1) * (w)
    nEdges = nRE + nBE
    
    """
        As edges have to be sorted, we must linearize them,
           which is transform the 2(+1)D pixels array
           into a 1D edges array.
        The question is:
            Do we linearize before or after building edges?
        Answer: 1D array of the pixels does not parallelize well...
            and building the edges on 2+1D replace the 3rd dim (RGB values)
            by the computed distance of the edge.
        Drawback: the edges are to be managed in CPU memory => less perf
    """
    # 3rd dim is for the "tuple" storing edge"s data
    edges = numpy.zeros(shape=(nEdges, 5), dtype=numpy.int32)
    
    computeEdges(picture, edges)
    
#    print(edges)
    
    return edges

@numba.jit
def computeEdges(picture, edges):
    h = picture.shape[0]
    w = picture.shape[1]
    
    i = 0
    # below edges
    for x in range(h-1):
        for y in range(w):
            generateEdge(picture, edges, i, x, y, x + 1, y)
            i += 1
    # right edges
    for x in range(h):
        for y in range(w - 1):
            generateEdge(picture, edges, i, x, y, x, y + 1)
            i += 1

# input: the coordinates of the 2 pixels
# output: a tuple with the 2 pixels' coordinates, their RGB vector
#   and their distance
@numba.jit
def generateEdge(picture, edges, i, x1, y1, x2, y2):
    # picture in uint8 => overflow problems
    r1 = picture[x1,y1,0]
    r2 = picture[x2,y2,0]
    g1 = picture[x1,y1,1]
    g2 = picture[x2,y2,1]
    b1 = picture[x1,y1,2]
    b2 = picture[x2,y2,2]
    
    dist = max(abs(r1 - r2), abs(g1 - g2), abs(b1 - b2))
    
    # we store the the coordinate of the pixels both with their distance
    edges[i, 0] = x1
    edges[i, 1] = y1
    edges[i, 2] = x2
    edges[i, 3] = y2
    edges[i, 4] = dist
