# SRMimpl

Python implementation on GPU of the Statistical Region Merging algorithm : R. Nock, F. Nielsen: Statistical Region Merging. IEEE Trans. Pattern Anal. Mach. Intell. 26(11): 1452-1458 (2004)

You have to install `numba` & `imageio` in order to execute the program.

## Running

You can launch the program with the command:

`python segmentPicture.py P01.JPG 10`

where:

- `P01.JPG` is an input image to be processed
- `10` is a value for the Q parameter of the algorithm
